import React from "react";
const notifier = require("node-notifier");

function App() {
  return (
    <>
      <h1>Hi</h1>
      <button
        onClick={() => {
          notifier.notify({
            title: "My notification",
            message: "Hello, there!",
          });
        }}
      >
        Notification
      </button>
    </>
  );
}

export default App;
